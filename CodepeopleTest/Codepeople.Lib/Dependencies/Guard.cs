﻿using System;

namespace Codepeople.Lib.Dependencies
{
    public static class Guard
    {
        public static void AssertNotNull(object input, string paramname)
        {
            if (input == null)
                throw new ArgumentNullException("Guard Assertion: " + paramname);
        }

        public static void AssertNotNullOrEmpty(object input, string paramname)
        {
            if (input == null || string.IsNullOrEmpty(input.ToString()))
                throw new ArgumentNullException("Guard Assertion: " + paramname);
        }
    }
}