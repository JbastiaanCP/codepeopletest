﻿using System.Threading;
using System.Threading.Tasks;

namespace Codepeople.Lib.Dependencies
{
    public interface IDossierRepository
    {
        Task<Dossier> GetDossierAsync(
            int dossiernummer,
            CancellationToken cancellationToken = default);
    }
}
