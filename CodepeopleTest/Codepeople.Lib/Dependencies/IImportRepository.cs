﻿using System.Threading;
using System.Threading.Tasks;

namespace Codepeople.Lib.Dependencies
{
    public interface IImportRepository
    {
        Task<ImportType> GetImportreferentieVoorOpdrachtgeverAsync(
            string code,
            CancellationToken cancellationToken = default);
    }
}
