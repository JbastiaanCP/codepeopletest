﻿namespace Codepeople.Lib.Dependencies
{
    public class ImportType
    {
        protected ImportType()
        {
        }

        public int? OpdrachtgeverNummerVoorAutomatischImporteren { get; protected set; }
    }
}