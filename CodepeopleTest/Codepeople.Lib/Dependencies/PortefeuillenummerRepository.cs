﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Codepeople.Lib.Dependencies
{
    public interface IPortefeuillenummerRepository
    {
        Task<IEnumerable<int>> GetPortefeuillesVoorOpdrachtgevernummerAsync(
            int opdrachtgevernummer,
            CancellationToken cancellationToken = default);
    }
}