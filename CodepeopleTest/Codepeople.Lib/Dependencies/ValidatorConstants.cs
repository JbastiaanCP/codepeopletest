﻿namespace Codepeople.Lib.Dependencies
{
    public static class ValidatorConstants
    {
        public static class ErrorMessages
        {
            public const string InvalidValue = "'{PropertyValue}' is an invalid value.";
            public const string IsRequired = "'{PropertyName}' is required.";
            public const string LengthBetween = "'{{PropertyName}}' should have a length with a minimum of {0} and a maximum of {1}.";
            public const string LengthExactly = "'{{PropertyName}}' should have a length of {0}.";
            public const string ValueBetween = "'{{PropertyName}}' should have a minimum value of {0} and a maximum of {1}";
        }
    }
}