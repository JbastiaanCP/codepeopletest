﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Codepeople.Lib.Dependencies;
using FluentValidation.Validators;

namespace Codepeople.Lib.SUT
{
    public class IsDossiernummerVanOpdrachtgeverValidator : AsyncValidatorBase
    {
        private readonly IImportRepository _importRepository;
        private readonly IDossierRepository _dossierRepository;
        private readonly IPortefeuillenummerRepository _portefeuillenummerRepository;
        private readonly string _importcodeOpdrachtgever;

        public IsDossiernummerVanOpdrachtgeverValidator(
            [NotNull] IImportRepository importRepository,
            [NotNull] IDossierRepository dossierRepository,
            [NotNull] IPortefeuillenummerRepository portefeuillenummerRepository,
            [NotNull] string importcodeOpdrachtgever)
            : base(ValidatorConstants.ErrorMessages.InvalidValue)
        {
            Guard.AssertNotNull(importRepository, nameof(importRepository));
            Guard.AssertNotNull(dossierRepository, nameof(dossierRepository));
            Guard.AssertNotNull(portefeuillenummerRepository, nameof(portefeuillenummerRepository));
            Guard.AssertNotNullOrEmpty(importcodeOpdrachtgever, nameof(importcodeOpdrachtgever));

            _importRepository = importRepository;
            _dossierRepository = dossierRepository;
            _portefeuillenummerRepository = portefeuillenummerRepository;
            _importcodeOpdrachtgever = importcodeOpdrachtgever;
        }

        protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellationToken)
        {
            if (int.TryParse(context.PropertyValue.ToString(), out var dossiernummer))
            {
                var opdrachtgevernummer =
                    await _importRepository.GetImportreferentieVoorOpdrachtgeverAsync(_importcodeOpdrachtgever, cancellationToken);
                var ownedportefeuilles = await _portefeuillenummerRepository
                    .GetPortefeuillesVoorOpdrachtgevernummerAsync(
                        opdrachtgevernummer.OpdrachtgeverNummerVoorAutomatischImporteren.GetValueOrDefault(),
                        cancellationToken);
                var dossier = await _dossierRepository.GetDossierAsync(
                    dossiernummer,
                    cancellationToken);

                if (ownedportefeuilles.Contains(dossier.Portefeuille.Portefeuillenummer))
                {
                    return true;
                }
            }

            return false;
        }
    }
}